package com.example.gsm_no1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.gsm_no1.data.Station;
import com.example.gsm_no1.data.StationAdapter;
import com.example.gsm_no1.data.StationViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDIT_NOTE_REQUEST = 2;

    public StationViewModel stationViewModel;

    private static final int MY_PERMISSIONS_REQUEST_RECEIVE_SMS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //SMS Permission

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)) {
                //Do nothing
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS}, MY_PERMISSIONS_REQUEST_RECEIVE_SMS);
            }

        }

        FloatingActionButton buttonAddNote = findViewById(R.id.addNewStationBtn);
        buttonAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST);
            }
        });

        FloatingActionButton buttonSupport = findViewById(R.id.supportBtn);
        buttonSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SupportActivity.class);
                startActivity(intent);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final StationAdapter adapter = new StationAdapter();
        recyclerView.setAdapter(adapter);

        stationViewModel = ViewModelProviders.of(this).get(StationViewModel.class);
        stationViewModel.getAllNotes().observe(this, new Observer<List<Station>>() {
            @Override
            public void onChanged(@Nullable List<Station> stations) {
                adapter.setNotes(stations);
            }
        });


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                stationViewModel.delete(adapter.getNoteAt(viewHolder.getAdapterPosition()));
                Toast.makeText(MainActivity.this, "Station gelöscht", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(recyclerView);

        //When clicking on item do: added AddEditNoteAct. to DataStationAct.
        adapter.setOnItemClickListener(new StationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Station station) {
                Intent intent = new Intent(MainActivity.this, DataStationActivity.class);
                intent.putExtra(DataStationActivity.EXTRA_ID, station.getId());
                intent.putExtra(DataStationActivity.EXTRA_TITLE, station.getTitle());
                intent.putExtra(DataStationActivity.EXTRA_DESCRIPTION, station.getDescription());
                intent.putExtra(DataStationActivity.EXTRA_LOCATION, station.getLocation());
                //intent.putExtra(DataStationActivity.EXTRA_PRIORITY, note.getPriority());
                startActivityForResult(intent, EDIT_NOTE_REQUEST);
                //startActivity(intent);
            }
        });
    }

    //give permission to transfer sms
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_RECEIVE_SMS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Erlaubnis erteilt", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Keine Erlaubnis erteilt", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_NOTE_REQUEST && resultCode == RESULT_OK) {
            String title = data.getStringExtra(AddEditNoteActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEditNoteActivity.EXTRA_DESCRIPTION);
            String location = data.getStringExtra(DataStationActivity.EXTRA_LOCATION);

            Station station = new Station(title, description, location);
            stationViewModel.insert(station);

            Toast.makeText(this, "Station hinzugefügt!", Toast.LENGTH_SHORT).show();
        } else if (requestCode == EDIT_NOTE_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(DataStationActivity.EXTRA_ID, -2); //change default value back to -1

            if (id == -1) {
                Toast.makeText(this, "Station kann nicht aktualisiert werden", Toast.LENGTH_SHORT).show();
                return;
            }

            String title = data.getStringExtra(DataStationActivity.EXTRA_TITLE);
            String description = data.getStringExtra(DataStationActivity.EXTRA_DESCRIPTION);
            String location = data.getStringExtra(DataStationActivity.EXTRA_LOCATION);

            Station station = new Station(title, description, location);
            station.setId(id);
            stationViewModel.update(station);

            Toast.makeText(this, "Station aktualisiert", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Station nicht geändert", Toast.LENGTH_SHORT).show();
        }
    }
}
