package com.example.gsm_no1;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.gsm_no1.data.StationViewModel;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DataStationActivity extends AppCompatActivity {
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    public static final String EXTRA_ID =
            "EXTRA_ID";
    public static final String EXTRA_TITLE =
            "EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION =
            "EXTRA_DESCRIPTION";
    public static final String EXTRA_LOCATION =
            "EXTRA_LOCATION";
    public static final int EDIT_NOTE_REQUEST = 2;

    private TextView editTextTitle, editTextDescription, editTextLocation, messageTV;
    private Button buttonRequest, buttonConnect;
    private FloatingActionButton buttonEdit;

    String phonenumber;
    String title;
    String id;
    String location;

    MyReceiver receiver = new MyReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            super.onReceive(context, intent);
            messageTV.setText("Letzte Temperatur: " + msg);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(SMS_RECEIVED));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_NOTE_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddEditNoteActivity.EXTRA_ID, -1);

            if (id == -1) {
                Toast.makeText(this, "Station kann nicht aktualisiert werden", Toast.LENGTH_SHORT).show();
                return;
            }


            String title = data.getStringExtra(AddEditNoteActivity.EXTRA_TITLE);
            String description = data.getStringExtra(AddEditNoteActivity.EXTRA_DESCRIPTION);
            String location = data.getStringExtra(AddEditNoteActivity.EXTRA_LOCATION);

            editTextTitle.setText(title);
            editTextDescription.setText(description);
            editTextLocation.setText(location);


            Toast.makeText(this, "Station aktualisiert", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Station nicht gespeichert", Toast.LENGTH_SHORT).show();
        }


    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.data_station);


        messageTV = findViewById(R.id.MessageTextView);
        buttonRequest = findViewById(R.id.requestBtn);
        buttonConnect = findViewById(R.id.pingBtn);
        editTextDescription = findViewById(R.id.RufnummerTextView);
        editTextTitle = findViewById(R.id.NameTextView);
        editTextLocation = findViewById(R.id.PrioTextView);


        Intent intent = getIntent();

        if (intent.hasExtra(EXTRA_ID)) {

            editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            editTextDescription.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
            editTextLocation.setText(intent.getStringExtra(EXTRA_LOCATION));

        }

        //Define Data from DB

        id = intent.getStringExtra(EXTRA_ID);
        title = intent.getStringExtra(EXTRA_TITLE);
        phonenumber = intent.getStringExtra(EXTRA_DESCRIPTION);
        location = intent.getStringExtra(EXTRA_LOCATION);


        buttonEdit = findViewById(R.id.editStationBtn);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                Intent intent = new Intent(DataStationActivity.this, AddEditNoteActivity.class);

                //transfer current data
                intent.putExtra(AddEditNoteActivity.EXTRA_ID, id);
                intent.putExtra(AddEditNoteActivity.EXTRA_TITLE, title);
                intent.putExtra(AddEditNoteActivity.EXTRA_DESCRIPTION, phonenumber);
                intent.putExtra(AddEditNoteActivity.EXTRA_LOCATION, location);
                startActivityForResult(intent, EDIT_NOTE_REQUEST);
            }
        });


        buttonRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMS();
            }
        });

        buttonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnection();

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save_note_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_note:
                saveEditNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void saveEditNote() {
        String title = editTextTitle.getText().toString();
        String description = editTextDescription.getText().toString();
        String location = editTextLocation.getText().toString();


        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(this, "Kein Inhalt eingetragen", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_LOCATION, location);
        //data.putExtra(EXTRA_PRIORITY, priority);

        int id = getIntent().getIntExtra(EXTRA_ID, -2); //new solution needed (always default)
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();

    }


    //check if device is connected to arduino
    private static final int PERMISSION_CONNECT = 1;

    public void checkConnection() {

        //check permission
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.SEND_SMS)
                    == PackageManager.PERMISSION_DENIED) {

                Log.d("permission", "permission denied to SEND_SMS - requesting it");
                String[] permissions = {Manifest.permission.SEND_SMS};

                requestPermissions(permissions, PERMISSION_CONNECT);
            }
        }


        String messageToSend = "CONNECT"; //defined call for arduino
        String number = phonenumber; //Number of arduino

        SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null, null);
        Toast.makeText(this, "SMS gesendet", Toast.LENGTH_LONG).show();
    }


    //send Message to arduino for request data
    private static final int PERMISSION_REQUEST_CODE = 1;

    public void sendSMS() {

        //check permission
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.SEND_SMS)
                    == PackageManager.PERMISSION_DENIED) {

                Log.d("permission", "permission denied to SEND_SMS - requesting it");
                String[] permissions = {Manifest.permission.SEND_SMS};

                requestPermissions(permissions, PERMISSION_REQUEST_CODE);
            }
        }

        String messageToSend = "REQUEST"; //defined call for arduino
        String number = phonenumber; //Number of arduino

        SmsManager.getDefault().sendTextMessage(number, null, messageToSend, null, null);
        Toast.makeText(this, "Daten werden angefragt", Toast.LENGTH_LONG).show();
    }

}





