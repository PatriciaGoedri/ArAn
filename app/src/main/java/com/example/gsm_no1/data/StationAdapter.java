package com.example.gsm_no1.data;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gsm_no1.R;

import java.util.ArrayList;
import java.util.List;

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.NoteHolder> {
    private List<Station> stations = new ArrayList<>();
    private OnItemClickListener listener;

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);
        return new NoteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        Station currentStation = stations.get(position);
        holder.textViewTitle.setText(currentStation.getTitle());
        holder.textViewDescription.setText(currentStation.getDescription());
        holder.textViewLocation.setText(currentStation.getLocation());

    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    public void setNotes(List<Station> stations) {
        this.stations = stations;
        notifyDataSetChanged();
    }

    public Station getNoteAt(int position) {
        return stations.get(position);
    }

    class NoteHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDescription;
        // private TextView textViewPriority;
        private TextView textViewLocation;

        public NoteHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            // textViewPriority = itemView.findViewById(R.id.text_view_priority);
            textViewLocation = itemView.findViewById(R.id.text_view_location);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(stations.get(position));
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Station station);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}