package com.example.gsm_no1.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class StationViewModel extends AndroidViewModel {
    private StationRepository repository;
    private LiveData<List<Station>> allNotes;

    public StationViewModel(@NonNull Application application) {
        super(application);
        repository = new StationRepository(application);
        allNotes = repository.getAllNotes();
    }

    public void insert(Station station) {
        repository.insert(station);
    }

    public void update(Station station) {
        repository.update(station);
    }

    public void delete(Station station) {
        repository.delete(station);
    }

    public void deleteAllNotes() {
        repository.deleteAllNotes();
    }

    public LiveData<List<Station>> getAllNotes() {
        return allNotes;
    }
}
