package com.example.gsm_no1.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "note_table")
public class Station {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;

    private String description;

    private String location;

    //private int priority;

    public Station(String title, String description, String location) {
        this.title = title;
        this.description = description;
        this.location = location;
        //this.priority = priority;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation(){
        return location;
    }

   /* public int getPriority() {
        return priority;
    }*/
}