package com.example.gsm_no1.data;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class StationRepository {
    private StationDao stationDao;
    private LiveData<List<Station>> allNotes;

    public StationRepository(Application application) {
        StationDatabase database = StationDatabase.getInstance(application);
        stationDao = database.noteDao();
        allNotes = stationDao.getAllNotes();
    }

    public void insert(Station station) {
        new InsertNoteAsyncTask(stationDao).execute(station);
    }

    public void update(Station station) {
        new UpdateNoteAsyncTask(stationDao).execute(station);
    }

    public void delete(Station station) {
        new DeleteNoteAsyncTask(stationDao).execute(station);
    }

    public void deleteAllNotes() {
        new DeleteAllNotesAsyncTask(stationDao).execute();
    }

    public LiveData<List<Station>> getAllNotes() {
        return allNotes;
    }

    private static class InsertNoteAsyncTask extends AsyncTask<Station, Void, Void> {
        private StationDao stationDao;

        private InsertNoteAsyncTask(StationDao stationDao) {
            this.stationDao = stationDao;
        }

        @Override
        protected Void doInBackground(Station... stations) {
            stationDao.insert(stations[0]);
            return null;
        }
    }

    private static class UpdateNoteAsyncTask extends AsyncTask<Station, Void, Void> {
        private StationDao stationDao;

        private UpdateNoteAsyncTask(StationDao stationDao) {
            this.stationDao = stationDao;
        }

        @Override
        protected Void doInBackground(Station... stations) {
            stationDao.update(stations[0]);
            return null;
        }
    }

    private static class DeleteNoteAsyncTask extends AsyncTask<Station, Void, Void> {
        private StationDao stationDao;

        private DeleteNoteAsyncTask(StationDao stationDao) {
            this.stationDao = stationDao;
        }

        @Override
        protected Void doInBackground(Station... stations) {
            stationDao.delete(stations[0]);
            return null;
        }
    }

    private static class DeleteAllNotesAsyncTask extends AsyncTask<Void, Void, Void> {
        private StationDao stationDao;

        private DeleteAllNotesAsyncTask(StationDao stationDao) {
            this.stationDao = stationDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            stationDao.deleteAllNotes();
            return null;
        }
    }
}
